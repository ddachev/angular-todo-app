import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "./store/app-state";
import { refreshTodosRequest } from "./store/actions/all-todos.actions";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor(private readonly store: Store<AppState>) {}
  title = "todo-app";

  ngOnInit(): void {
    this.store.dispatch(refreshTodosRequest());
  }
}
