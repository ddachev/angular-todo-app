import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CreateTodoComponent } from "./create-todo.component";
import { provideMockStore, MockStore } from "@ngrx/store/testing";
import { MaterialModule } from "src/app/material.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { createAction, props } from "@ngrx/store";
import { NewTodo } from "src/app/data/schema/new-todo";

describe("CreateTodoComponent", () => {
  let component: CreateTodoComponent;
  let fixture: ComponentFixture<CreateTodoComponent>;
  let template;
  let mockStore: MockStore;

  beforeEach(async () => {
    const initialState = {
      todos: null
    };
    await TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [CreateTodoComponent],
      providers: [
        provideMockStore({ initialState }),
      ]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CreateTodoComponent);
        mockStore = TestBed.inject(MockStore);
        component = fixture.componentInstance;
        template = fixture.nativeElement;
      });
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  describe("CreateTodo()", () => {
    it("should call createTodo() on create-btn click", () => {
      // Arrange
      spyOn(component, "createTodo");
      // tslint:disable-next-line: prefer-const
      let button = fixture.debugElement.nativeElement.querySelector("button");
      // Act
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(component.createTodo).toHaveBeenCalled();
      });
    });

    it("should call store.dispatch()", () => {
      // Arrange
      const todo: NewTodo = { title: "TEST", description: "TEST" };
      const createTodoRequest = createAction(
        "[Todo] Create Todo",
        props<{ todo: NewTodo }>()
      );
      spyOn(component, "createTodo");
      const spy = jest.spyOn(mockStore, "dispatch");
      mockStore.dispatch(createTodoRequest({ todo }));
      // tslint:disable-next-line: prefer-const
      let button = fixture.debugElement.nativeElement.querySelector("button");
      // Act
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(spy).toHaveBeenCalled();
      });
    });

    it("should call store.dispatch() with correct value", () => {
      // Arrange
      const todo: NewTodo = { title: "TEST", description: "TEST" };
      const createTodoRequest = createAction(
        "[Todo] Create Todo",
        props<{ todo: NewTodo }>()
      );
      spyOn(component, "createTodo");
      const spy = jest.spyOn(mockStore, "dispatch");
      mockStore.dispatch(createTodoRequest({ todo }));
      // tslint:disable-next-line: prefer-const
      let button = fixture.debugElement.nativeElement.querySelector("button");
      // Act
      button.click();
      fixture.detectChanges();
      // Assert
      fixture.whenStable().then(() => {
        expect(spy).toHaveBeenCalledWith(createTodoRequest({ todo }));
      });
    });
  });
});
