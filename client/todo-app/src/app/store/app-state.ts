import { Todo } from '../data/schema/todo';

export interface AppState {
  todos: Todo[];
}
