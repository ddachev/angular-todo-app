import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "src/app/material.module";
import { EditTodoComponent } from "./edit-todo.component";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { Todo } from "src/app/data/schema/todo";

describe("EditTodoComponent", () => {
  let component: EditTodoComponent;
  let fixture: ComponentFixture<EditTodoComponent>;
  let templete;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          FormsModule,
          ReactiveFormsModule,
          MaterialModule,
          BrowserAnimationsModule
        ],
        declarations: [EditTodoComponent],
        providers: [{ provide: MAT_DIALOG_DATA, useValue: {} }],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTodoComponent);
    component = fixture.componentInstance;
    templete = fixture.nativeElement;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  describe("EditTodo()", () => {
    it("should call emit() on updatedTodo", () => {
      // Arrange
      const spy = spyOn(component.updatedTodo, "emit");
      // Act
      component.updateTodo();
      // Assert
      expect(spy).toHaveBeenCalled();
    });
    it("should call emit() on updatedTodo with correct arguments", () => {
      component.todo = {
        id: 5
      } as Todo;
      // Arrange
      const spy = spyOn(component.updatedTodo, "emit");
      // Act
      component.updateTodo();
      // Assert
      expect(spy).toHaveBeenCalled();
    });
  });
});
