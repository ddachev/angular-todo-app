import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import {
  completeTodoRequest,
  deleteTodoRequest,
  updateTodoRequest
} from "src/app/store/actions/all-todos.actions";

import { AllTodosComponent } from "./all-todos.component";

describe("AllTodosComponent", () => {
  let component: AllTodosComponent;
  let fixture: ComponentFixture<AllTodosComponent>;
  let mockStore: MockStore;
  let templete;
  let initialState = {
    todos: []
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          // MaterialModule,
        ],
        declarations: [AllTodosComponent],
        providers: [provideMockStore({ initialState })],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AllTodosComponent);
    mockStore = TestBed.inject(MockStore);
    component = fixture.componentInstance;
    templete = fixture.nativeElement;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("deleteTodo() should call store.dispatch()", () => {
    //Arrange
    const spy = spyOn(mockStore, "dispatch");
    //Assert
    component.deleteTodo(5);
    //Act
    expect(spy).toHaveBeenCalled();
  });
  it("deleteTodo() should call store.dispatch() with correct arguments", () => {
    //Arrange
    const spy = spyOn(mockStore, "dispatch");
    const todoId = 5;
    //Assert
    component.deleteTodo(5);
    //Act
    expect(spy).toHaveBeenCalledWith(deleteTodoRequest({ todoId }));
  });

  it("updateTodo() should call store.dispatch()", () => {
    //Arrange
    const spy = spyOn(mockStore, "dispatch");
    //Assert
    component.updateTodo({ title: "test", description: "test" }, 5);
    //Act
    expect(spy).toHaveBeenCalled();
  });
  it("updateTodo() should call store.dispatch() with correct arguments", () => {
    //Arrange
    const spy = spyOn(mockStore, "dispatch");
    const todo = { title: "test", description: "test" };
    const id = 5;
    //Assert
    component.updateTodo(todo, 5);
    //Act
    expect(spy).toHaveBeenCalledWith(updateTodoRequest({ todo, id }));
  });

  it("completeTodo() should call store.dispatch()", () => {
    //Arrange
    const spy = spyOn(mockStore, "dispatch");
    //Assert
    component.completeTodo(5);
    //Act
    expect(spy).toHaveBeenCalled();
  });
  it("completeTodo() should call store.dispatch() with correct arguments", () => {
    //Arrange
    const spy = spyOn(mockStore, "dispatch");
    const completedTodo = { completed: true };
    const id = 5;
    //Assert
    component.completeTodo(5);
    //Act
    expect(spy).toHaveBeenCalledWith(
      completeTodoRequest({ completedTodo, id })
    );
  });
});
