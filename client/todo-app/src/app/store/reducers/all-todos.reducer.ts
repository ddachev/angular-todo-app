import { createReducer, on } from "@ngrx/store";
import { Todo } from "src/app/data/schema/todo";
import {
  completeTodoDone,
  createTodoDone,
  deleteTodoDone,
  refreshTodosDone,
  updateTodoDone
} from "../actions/all-todos.actions";

export const allTodosReducer = createReducer<Todo[]>(
  [],
  on(refreshTodosDone, (_, action) => action.todos),
  on(createTodoDone, (state, action) => {
    return [...state, action.todo];
  }),
  on(deleteTodoDone, (state, action) => {
    return [...state].filter(t => t.id !== action.todoId);
  }),
  on(updateTodoDone, (state, action) => {
    return [...state].map(t => {
      if (t.id === action.todo.id) {
        return action.todo;
      }
      return t;
    });
  }),
  on(completeTodoDone, (state, action) => {
    return [...state].map(t => {
      if (t.id === action.todo.id) {
        return action.todo;
      }
      return t;
    });
  })
);
