import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Todo } from "../schema/todo";
import { NewTodo } from "../schema/new-todo";
import { UpdateTodo } from "../schema/update-todo";
import { CompleteTodo } from "../schema/complete-todo";

@Injectable({
  providedIn: "root"
})
export class TodoDataService {
  constructor(private readonly httpClient: HttpClient) {}

  getAllTodos(): Observable<Todo[]> {
    return this.httpClient.get<Todo[]>("http://localhost:1337/tasks");
  }

  getSingleTodo(id: number): Observable<Todo> {
    return this.httpClient.get<Todo>(`http://localhost:1337/tasks/${id}`);
  }

  createTodo(newTodo: NewTodo): Observable<Todo> {
    return this.httpClient.post<Todo>("http://localhost:1337/tasks", newTodo);
  }

  updateTodo(updateInfo: UpdateTodo, id: number): Observable<Todo> {
    return this.httpClient.patch<Todo>(
      `http://localhost:1337/tasks/${id}`,
      updateInfo
    );
  }

  completeTodo(updateInfo: CompleteTodo, id: number): Observable<Todo> {
    return this.httpClient.patch<Todo>(
      `http://localhost:1337/tasks/${id}`,
      updateInfo
    );
  }

  deleteTodo(id: number): Observable<Todo> {
    return this.httpClient.delete<Todo>(`http://localhost:1337/tasks/${id}`);
  }
}
