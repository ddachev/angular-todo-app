import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllTodosComponent } from '../layout/all-todos/all-todos.component';
import { CreateTodoComponent } from '../layout/create-todo/create-todo.component';


const routes: Routes = [
  { path: '', redirectTo: 'todos', pathMatch: 'full' },
  { path: 'todos', component: AllTodosComponent },
  { path: 'todos/new', component: CreateTodoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
