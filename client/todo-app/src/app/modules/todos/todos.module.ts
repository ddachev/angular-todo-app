import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CreateTodoComponent } from "src/app/layout/create-todo/create-todo.component";
import { MaterialModule } from "src/app/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AllTodosComponent } from "src/app/layout/all-todos/all-todos.component";
import { EditTodoComponent } from "src/app/layout/edit-todo/edit-todo/edit-todo.component";
import { SingleTodoComponent } from "src/app/layout/single-todo/single-todo.component";

@NgModule({
  declarations: [
    CreateTodoComponent,
    AllTodosComponent,
    EditTodoComponent,
    SingleTodoComponent
  ],
  imports: [CommonModule, MaterialModule, FormsModule, ReactiveFormsModule],
  exports: [
    CreateTodoComponent,
    AllTodosComponent,
    EditTodoComponent,
    SingleTodoComponent
  ]
})
export class TodosModule {}
