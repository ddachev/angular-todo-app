export class NewTodo {
  title: string;
  description: string;
  priority?: number;
  project?: any;
}
