import { Component, EventEmitter, Inject, Output } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UpdateTodo } from "src/app/data/schema/update-todo";
@Component({
  selector: "app-edit-todo",
  templateUrl: "./edit-todo.component.html",
  styleUrls: ["./edit-todo.component.css"]
})
export class EditTodoComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public todo: any) {}
  @Output()
  updatedTodo: EventEmitter<UpdateTodo> = new EventEmitter<UpdateTodo>();
  fb = new FormBuilder();
  form = this.fb.group({
    title: this.fb.control("", [Validators.required, Validators.minLength(2)]),
    description: this.fb.control("", [])
  });

  updateTodo(): void {
    const todo: UpdateTodo = this.form.value;
    this.updatedTodo.emit(todo);
  }
}
