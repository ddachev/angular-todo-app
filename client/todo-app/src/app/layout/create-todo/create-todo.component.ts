import { Component } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { NewTodo } from "src/app/data/schema/new-todo";
import { AppState } from "src/app/store/app-state";
import { createTodoRequest } from "src/app/store/actions/all-todos.actions";

@Component({
  selector: "app-create-todo",
  templateUrl: "./create-todo.component.html",
  styleUrls: ["./create-todo.component.css"]
})
export class CreateTodoComponent {
  constructor(private store: Store<AppState>) {}

  fb = new FormBuilder();
  form = this.fb.group({
    title: this.fb.control("", [Validators.required, Validators.minLength(2)]),
    description: this.fb.control("", [])
  });

  createTodo(): void {
    const todo: NewTodo = this.form.value;
    this.store.dispatch(createTodoRequest({ todo }));
    this.form.reset();
  }
}
