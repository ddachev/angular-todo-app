# Angular Todo App

Although it is a small app, I believe that the state management is of great importance.
This is why I approached the app by first setting up the state using the NgRx library.
The next step was to create all the components with their functionality and test them
using Jest. Regarding the design I am using Angular Material.

# How to run the client

Installing dependencies
- npm i

Running the client on http://localhost:4200/
- ng serve

Execute command in root folder for running unit tests
- ng test