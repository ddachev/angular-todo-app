import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output
} from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { Subscription } from "rxjs";
import { Todo } from "src/app/data/schema/todo";
import { UpdateTodo } from "src/app/data/schema/update-todo";
import { EditTodoComponent } from "../edit-todo/edit-todo/edit-todo.component";

@Component({
  selector: "app-single-todo",
  templateUrl: "./single-todo.component.html",
  styleUrls: ["./single-todo.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleTodoComponent implements OnDestroy {
  @Input() todo: Todo;
  @Output() deleteTodoId: EventEmitter<number>;
  @Output() completeTodoId: EventEmitter<number>;
  @Output() updateTodoData: EventEmitter<{ todo: UpdateTodo; id: number }>;
  private subscription: Subscription = new Subscription();
  constructor(public dialog: MatDialog) {
    this.deleteTodoId = new EventEmitter<number>();
    this.completeTodoId = new EventEmitter<number>();
    this.updateTodoData = new EventEmitter<{ todo: UpdateTodo; id: number }>();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  completeTodo(id: number): void {
    this.completeTodoId.emit(id);
  }
  deleteTodo(id: number): void {
    this.deleteTodoId.emit(id);
  }
  openDialog(id: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = this.todo;
    const dialogRef = this.dialog.open(EditTodoComponent, dialogConfig);
    this.subscription = dialogRef.componentInstance.updatedTodo.subscribe(
      todo => this.updateTodoData.emit({ todo, id })
    );
  }
}
