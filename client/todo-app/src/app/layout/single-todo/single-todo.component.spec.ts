import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { SingleTodoComponent } from "./single-todo.component";
import { of } from "rxjs";
import { Todo } from "src/app/data/schema/todo";

describe("SingleTodoComponent", () => {
  let component: SingleTodoComponent;
  let fixture: ComponentFixture<SingleTodoComponent>;
  let templete;
  const mockDialog = {
    open() {
      return {
        componentInstance: {
          updatedTodo: of({ title: "test", description: "test" })
        }
      };
    }
  };

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: [SingleTodoComponent],
        providers: [
          {
            provide: MatDialog,
            useValue: mockDialog
          }
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleTodoComponent);
    component = fixture.componentInstance;
    templete = fixture.nativeElement;
    component.todo = {} as Todo;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  describe("completeTodo()", () => {
    it("should call completeTodoId.emit()", () => {
      // Arrange
      const spy = spyOn(component.completeTodoId, "emit");
      // Act
      component.completeTodo(5);
      // Assert
      expect(spy).toHaveBeenCalled();
    });

    it("should call completeTodoId.emit() with correct arguments", () => {
      // Arrange
      component.todo = {
        id: 5
      } as Todo;
      // let button = fixture.debugElement.query(By.css(".complete-btn"));
      const spy = spyOn(component.completeTodoId, "emit");
      // Act
      component.completeTodo(5);
      // Assert
      expect(spy).toHaveBeenCalledWith(5);
    });
  });

  describe("deleteTodo()", () => {
    it("should call deleteTodoId.emit()", () => {
      // Arrange
      const spy = spyOn(component.deleteTodoId, "emit");
      // Act
      component.deleteTodo(5);
      // Assert
      expect(spy).toHaveBeenCalled();
    });

    it("should call deleteTodoId.emit() with correct arguments", () => {
      // Arrange
      const spy = spyOn(component.deleteTodoId, "emit");
      // Act
      component.deleteTodo(5);
      // Assert
      expect(spy).toHaveBeenCalledWith(5);
    });
  });
});
