export class UpdateTodo {
  title?: string;
  description?: string;
  priority?: number;
  project?: any;
}
