import { createAction, props } from "@ngrx/store";
import { CompleteTodo } from 'src/app/data/schema/complete-todo';
import { NewTodo } from "src/app/data/schema/new-todo";
import { Todo } from "src/app/data/schema/todo";
import { UpdateTodo } from "src/app/data/schema/update-todo";

export const refreshTodosRequest = createAction("[Todo] Refresh Todos Request");
export const refreshTodosDone = createAction(
  "[Todo] Refresh Todos Done",
  props<{ todos: Todo[] }>()
);

export const createTodoRequest = createAction(
  "[Todo] Create Todo",
  props<{ todo: NewTodo }>()
);
export const createTodoDone = createAction(
  "[Todo] Create Todo Done",
  props<{ todo: Todo }>()
);

export const updateTodoRequest = createAction(
  "[Todo] Update Todo",
  props<{
    todo: UpdateTodo;
    id: number;
  }>()
);
export const updateTodoDone = createAction(
  "[Todo] Update Todo Done",
  props<{
    todo: Todo;
  }>()
);
export const completeTodoRequest = createAction(
  "[Todo] Complete Todo",
  props<{
    completedTodo: CompleteTodo;
    id: number;
  }>()
);
export const completeTodoDone = createAction(
  "[Todo] Complete Todo Done",
  props<{
    todo: Todo;
  }>()
);

export const deleteTodoRequest = createAction(
  "[Todo] Delete Todo",
  props<{ todoId: number }>()
);
export const deleteTodoDone = createAction(
  "[Todo] Delete Todo Done",
  props<{ todoId: number }>()
);

