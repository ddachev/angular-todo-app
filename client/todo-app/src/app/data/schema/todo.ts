export class Todo {
  createdAt: number | Date;
  updatedAt: number | Date;
  id: number;
  title: string;
  description: string;
  priority?: number;
  project?: any;
  completed?: boolean;
}
