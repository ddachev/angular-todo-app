import { Injectable } from "@angular/core";
import { TodoDataService } from "src/app/data/services/todo-data.service";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import {
  completeTodoDone,
  completeTodoRequest,
  createTodoDone,
  createTodoRequest,
  deleteTodoDone,
  deleteTodoRequest,
  refreshTodosDone,
  refreshTodosRequest,
  updateTodoDone,
  updateTodoRequest
} from "../actions/all-todos.actions";
import { switchMap, map, catchError } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { NotificatorService } from "src/app/data/services/notificator.service";
import { Todo } from "src/app/data/schema/todo";

@Injectable()
export class AllTodosEffects {
  constructor(
    private readonly todosDataService: TodoDataService,
    private readonly actions$: Actions,
    private readonly notificator: NotificatorService
  ) {}

  refreshTodos$ = createEffect(() =>
    this.actions$.pipe(
      ofType(refreshTodosRequest),
      switchMap(() => {
        return this.todosDataService.getAllTodos().pipe(
          map(todos => {
            return refreshTodosDone({ todos });
          }),
          catchError(() => {
            console.log("REFRESH TODOS FAILED");
            return EMPTY;
          })
        );
      })
    )
  );

  createTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createTodoRequest),
      switchMap(action => {
        return this.todosDataService.createTodo(action.todo).pipe(
          map((todo: Todo) => {
            this.notificator.success(`You have successfully created a Todo!`);
            return createTodoDone({ todo });
          }),
          catchError(() => {
            console.log("CREATE TODO FAILED");
            return EMPTY;
          })
        );
      })
    )
  );

  updateTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateTodoRequest),
      switchMap(action => {
        return this.todosDataService.updateTodo(action.todo, action.id).pipe(
          map(todo => {
            this.notificator.success(`You have successfully modified a Todo!`);
            return updateTodoDone({ todo });
          }),
          catchError(() => {
            console.log("UPDATE TODO FAILED");
            return EMPTY;
          })
        );
      })
    )
  );

  completeTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(completeTodoRequest),
      switchMap(action => {
        return this.todosDataService
          .completeTodo(action.completedTodo, action.id)
          .pipe(
            map(todo => {
              this.notificator.success(
                `You have successfully completed a Todo!`
              );
              return completeTodoDone({ todo });
            }),
            catchError(() => {
              console.log("COMPLETE TODO FAILED");
              return EMPTY;
            })
          );
      })
    )
  );

  deleteTodo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteTodoRequest),
      switchMap(action => {
        return this.todosDataService.deleteTodo(action.todoId).pipe(
          map(todo => {
            const todoId = todo.id;
            this.notificator.success(`You have successfully deleted a Todo!`);
            return deleteTodoDone({ todoId });
          }),
          catchError(() => {
            console.log("DELETE TODO FAILED");
            return EMPTY;
          })
        );
      })
    )
  );
}
