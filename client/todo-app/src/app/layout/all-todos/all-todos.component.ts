import { Component } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Todo } from "src/app/data/schema/todo";
import { UpdateTodo } from "src/app/data/schema/update-todo";
import { AppState } from "src/app/store/app-state";
import {
  completeTodoRequest,
  deleteTodoRequest,
  updateTodoRequest
} from "src/app/store/actions/all-todos.actions";

@Component({
  selector: "app-all-todos",
  templateUrl: "./all-todos.component.html",
  styleUrls: ["./all-todos.component.css"]
})
export class AllTodosComponent {
  todos$: Observable<Todo[]>;
  constructor(private readonly store: Store<AppState>) {
    this.todos$ = this.store.pipe(
      select(state => state.todos),
      map(todos => {
        return todos.filter(todo => !todo.completed).map(t => {
          return {
            ...t,
            createdAt: new Date(t.createdAt),
            updatedAt: new Date(t.updatedAt)
          };
        });
      })
    );
  }

  deleteTodo(todoId: number): void {
    this.store.dispatch(deleteTodoRequest({ todoId }));
  }
  updateTodo(todo: UpdateTodo, id: number): void {
    this.store.dispatch(updateTodoRequest({ todo, id }));
  }
  completeTodo(id: number): void {
    const completedTodo = {
      completed: true
    };
    this.store.dispatch(completeTodoRequest({ completedTodo, id }));
  }
}
